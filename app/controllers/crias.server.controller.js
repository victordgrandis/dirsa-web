'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Cria = mongoose.model('Cria'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Cria', 'categoria');

module.exports = crud;