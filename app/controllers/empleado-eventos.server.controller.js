'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  EmpleadoEvento = mongoose.model('EmpleadoEvento'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('EmpleadoEvento', 'fecha');

module.exports = crud;