'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Empleado = mongoose.model('Empleado'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Empleado', 'nombre');

module.exports = crud;