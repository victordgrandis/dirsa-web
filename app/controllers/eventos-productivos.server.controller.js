'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  EventoProductivo = mongoose.model('EventoProductivo'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('EventoProductivo', 'codigo');

module.exports = crud;