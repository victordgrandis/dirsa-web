'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  EventoReproductivo = mongoose.model('EventoReproductivo'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('EventoReproductivo', 'fecha');

module.exports = crud;