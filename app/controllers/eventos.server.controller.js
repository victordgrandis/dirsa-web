'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Evento = mongoose.model('Evento'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Evento', 'codigo');

module.exports = crud;