'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Muestra = mongoose.model('Muestra'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Muestra', 'numero');

module.exports = crud;