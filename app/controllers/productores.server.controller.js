'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Productor = mongoose.model('Productor'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Productor', 'idProductor');

module.exports = crud;