'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Tambo = mongoose.model('Tambo'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Tambo', 'idTambo');

module.exports = crud;