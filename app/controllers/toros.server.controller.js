'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Toro = mongoose.model('Toro'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Toro', 'hbaToro');

module.exports = crud;