'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Tratamiento = mongoose.model('Tratamiento'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Tratamiento', 'numero');

module.exports = crud;