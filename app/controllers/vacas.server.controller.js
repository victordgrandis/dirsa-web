'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('./errors.server.controller'),
  Vaca = mongoose.model('Vaca'),
    _ = require('lodash');

var crud = require('./crud.server.controller')('Vaca', 'rp');

module.exports = crud;