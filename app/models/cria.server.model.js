'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Cria Schema
 */
var CriaSchema = new Schema({
	rpAbuela: {
		type: Number
	},
	rpMadre: {
		type: Number
	},
	hbaPadre: {
		type: Number
	},
	// Atributos de Animal
	fechaNacimiento: {
		type: Date
	},
	fechaIngreso: {
		type: Date
	},
	edad: {
		type: Number
	},
	nombre: {
		type: String,
		default: '',
		trim: true,
	},
	raza: {
		type: String,
		default: '',
		trim: true,
	},
	rp: {
		type: Number
	},
	categoria: {
		type: String,
		default: '',
		trim: true
	},
	activa: {
		type: Boolean
	},
	sexo: {
		type: String,
		default: '',
		trim: true
	}
});

mongoose.model('Cria', CriaSchema);