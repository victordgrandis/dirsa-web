'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * EmpleadoEvento Schema
 */
var EmpleadoEventoSchema = new Schema({
	empleado:{
		type: Schema.Types.ObjectId,
		ref: 'Empleado'
	},
	eventoReproductivo:{
		type: Schema.Types.ObjectId,
		ref: 'EventoReproductivo'
	},
	rol:{
		type: String,
		trim: true,
		default: ''
	},
	fecha:{
		type: Date
	}
});

mongoose.model('EmpleadoEvento', EmpleadoEventoSchema);