'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Empleado Schema
 */
var EmpleadoSchema = new Schema({
	nombre:{
		type: String,
		trim: true,
	},
	direccion:{
		type: String,
		trim: true,
		default: ''
	},
	antiguedad:{
		type: Number
	}
});

mongoose.model('Empleado', EmpleadoSchema);