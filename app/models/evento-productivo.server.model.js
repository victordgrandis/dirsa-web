'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * EventoProductivo Schema
 */
var EventoProductivoSchema = new Schema({
	vaca:{
		type: Schema.Types.ObjectId,
		ref: 'Vaca'
	},
	estadoCorporal:{
		type: Number
	},
	fecha:{
		type: Date
	},
	leche:{
		type: Number
	},
	numeroOrdeñe:{
		type: Number
	}
});

mongoose.model('EventoProductivo', EventoProductivoSchema);