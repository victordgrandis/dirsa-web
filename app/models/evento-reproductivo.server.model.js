'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * EventoReproductivo Schema
 */
var EventoReproductivoSchema = new Schema({
	evento:{
		type: Schema.Types.ObjectId,
		ref: 'Evento'
	},
	vaca:{
		type: Number
	},
	codigo: {
		type: Number
	},
	numero: {
		type: Number
	},
	fecha: {
		type: Date
	},
	participante: {
		type: String,
		trim: true,
		default: ''
	},
	tratamiento: {
		type: String,
		trim: true,
		default: ''
	},
	tambo:{
		type: Schema.Types.ObjectId,
		ref: 'Tambo'
	},
	toro:{
		type: Number
	}
});

mongoose.model('EventoReproductivo', EventoReproductivoSchema);