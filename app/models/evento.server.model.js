'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Evento Schema
 */
var EventoSchema = new Schema({
	codigo:{
		type: Number
	},
	tipo:{
		type: Number
	},
	descripcion:{
		type: String,
		trim: true,
		default: ''
	},
	color:{
		type: String,
		trim: true,
		default: ''
	},
	masivo: {
		type: Boolean
	}
});

mongoose.model('Evento', EventoSchema);