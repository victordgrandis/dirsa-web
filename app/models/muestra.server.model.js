'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Muestra Schema
 */
var MuestraSchema = new Schema({
	numero:{
		type: Number
	},
	evento:{
		type: Schema.Types.ObjectId,
		ref: 'EventoProductivo'
	},
	grasa:{
		type: Number
	},
	leche:{
		type: Number
	},
	proteinas:{
		type: Number
	},
	recuentoCelular:{
		type: Number
	},
	solidos:{
		type: Number
	}
});

mongoose.model('Muestra', MuestraSchema);