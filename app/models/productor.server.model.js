'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Productor Schema
 */
var ProductorSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		trim: true,
	},
	idProductor: {
		type: Number
	},
	direccion: {
		type: String,
		default: '',
		trim: true
	},
	provincia: {
		type: String,
		default: '',
		trim: true,
	},
	telefono: {
		type: String,
		default: '',
		trim: true,
	}
});

mongoose.model('Productor', ProductorSchema);