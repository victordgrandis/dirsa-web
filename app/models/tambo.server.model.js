'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Tambo Schema
 */
var TamboSchema = new Schema({
	productor: {
		type: Schema.Types.ObjectId,
		ref: 'Productor'
	},
	nombre: {
		type: String,
		default: '',
		trim: true
	},
	direccion: {
		type: String,
		default: '',
		trim: true
	},
	fechaAlta: {
		type: Date,
		trim: true
	},
	idTambo: {
		type: Number
	},
	idAcha: {
		type: Number
	}
});

mongoose.model('Tambo', TamboSchema);