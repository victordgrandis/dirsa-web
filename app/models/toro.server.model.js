'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Toro Schema
 */
var ToroSchema = new Schema({
	apodo: {
		type: String,
		default: '',
		trim: true,
	},
	hbaToro: {
		type: Number,
		trim: true,
	},
	// Atributos de Animal
	fechaNacimiento: {
		type: Date
	},
	fechaIngreso: {
		type: Date
	},
	edad: {
		type: Number
	},
	nombre: {
		type: String,
		default: '',
		trim: true,
	},
	raza: {
		type: String,
		default: '',
		trim: true,
	},
	rp: {
		type: Number
	},
	tipoServicio: {
		type: String,
		default: '',
		trim: true
	},
	servicios: [{
		type: String
	}],
	preñeces: [{
		evento: {type: String},
		servicio: {type: String}
	}]
});

mongoose.model('Toro', ToroSchema);