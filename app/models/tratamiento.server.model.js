'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Tratamiento Schema
 */
var TratamientoSchema = new Schema({
	numero:{
		type: Number
	},
	descripcion:{
		type: String,
		trim: true,
		default:''
	}
});

mongoose.model('Tratamiento', TratamientoSchema);