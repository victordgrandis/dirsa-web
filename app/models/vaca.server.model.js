'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Vaca Schema
 */
var VacaSchema = new Schema({
	tambo:{
		type: Schema.Types.ObjectId,
		ref: 'Tambo'
	},
	rpMadre: {
		type: Number
	},
	hbaPadre:{
		type: Number
	},
	cantidadServicios: {
		type: Number
	},
	caravana: {
		type: Number
	},
	diasAlParto: {
		type: Number
	},
	diasPrenez: {
		type: Number
	},
	fechaInicioLactancia: {
		type: Date,
		trim: true
	},
	numeroLactancia: {
		type: Number
	},
	fechaSecado: {
		type: Date,
		trim: true
	},
	ipp: {
		type: Number
	},
	ippp: {
		type: Number
	},
	ipc: {
		type: Number
	},
	indiceFertilidad: {
		type: Number
	},
	loteActual: {
		type: Number
	},
	loteAnterior: {
		type: Number
	},
	proximoSecado: {
		type: Date,
		trim: true
	},
	diasSeca: {
		type: Number
	},
	noRetorno: {
		type: String,
		trim: true,
		default: ''
	},
	proximoParto: {
		type: Date,
		trim: true
	},
	tipoAnimal: {
		type: String,
		trim: true,
		default: 'Q'
	},
	// Atributos de Animal
	fechaNacimiento: {
		type: Date,
		trim: true
	},
	fechaIngresoSistema: {
		type: Date,
		trim: true
	},
	fechaIngresoTambo: {
		type: Date,
		trim: true
	},
	edad: {
		type: Number
	},
	nombre: {
		type: String,
		default: '',
		trim: true,
	},
	raza: {
		type: String,
		default: '',
		trim: true,
	},
	rp: {
		type: Number
	},
	categoria: {
		type: String,
		default: '',
		trim: true
	},
	activa: {
		type: Boolean
	}
});

mongoose.model('Vaca', VacaSchema);