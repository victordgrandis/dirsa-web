'use strict';

module.exports = function(app) {
	var crias = require('../controllers/crias.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/crias')
		.get(crias.list)
		.post(apiAuth, users.requiresLogin, crias.create);

	app.route('/crias/:criaId')
		.get(crias.read)
		.put(apiAuth, users.requiresLogin, crias.update)
		.delete(apiAuth, users.requiresLogin, crias.delete);

	// Finish by binding the article middleware
	app.param('criaId', crias.getByID);
};