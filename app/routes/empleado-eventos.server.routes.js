'use strict';

module.exports = function(app) {
	var empleadoEventos = require('../controllers/empleado-eventos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/empleadoEventos')
		.get(empleadoEventos.list)
		.post(apiAuth, users.requiresLogin, empleadoEventos.create);

	app.route('/empleadoEventos/:empleadoEventoId')
		.get(empleadoEventos.read)
		.put(apiAuth, users.requiresLogin, empleadoEventos.update)
		.delete(apiAuth, users.requiresLogin, empleadoEventos.delete);

	// Finish by binding the article middleware
	app.param('empleadoEventoId', empleadoEventos.getByID);
};