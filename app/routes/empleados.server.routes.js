'use strict';

module.exports = function(app) {
	var empleados = require('../controllers/empleados.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/empleados')
		.get(empleados.list)
		.post(apiAuth, users.requiresLogin, empleados.create);

	app.route('/empleados/:empleadoId')
		.get(empleados.read)
		.put(apiAuth, users.requiresLogin, empleados.update)
		.delete(apiAuth, users.requiresLogin, empleados.delete);

	// Finish by binding the article middleware
	app.param('empleadoId', empleados.getByID);
};