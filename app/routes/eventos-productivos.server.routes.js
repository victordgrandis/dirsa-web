'use strict';

module.exports = function(app) {
	var eventosProductivos = require('../controllers/eventos-productivos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/eventosProductivos')
		.get(eventosProductivos.list)
		.post(apiAuth, users.requiresLogin, eventosProductivos.create);

	app.route('/eventosProductivos/:eventoProductivoId')
		.get(eventosProductivos.read)
		.put(apiAuth, users.requiresLogin, eventosProductivos.update)
		.delete(apiAuth, users.requiresLogin, eventosProductivos.delete);

	// Finish by binding the article middleware
	app.param('eventoProductivoId', eventosProductivos.getByID);
};