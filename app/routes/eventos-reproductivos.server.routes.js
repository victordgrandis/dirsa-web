'use strict';

module.exports = function(app) {
	var eventosReproductivos = require('../controllers/eventos-reproductivos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/eventos-reproductivos')
		.get(eventosReproductivos.list)
		.post(apiAuth, users.requiresLogin, eventosReproductivos.create);

	app.route('/eventos-reproductivos/:eventoReproductivoId')
		.get(eventosReproductivos.read)
		.put(apiAuth, users.requiresLogin, eventosReproductivos.update)
		.delete(apiAuth, users.requiresLogin, eventosReproductivos.delete);

	// Finish by binding the article middleware
	app.param('eventoReproductivoId', eventosReproductivos.getByID);
};