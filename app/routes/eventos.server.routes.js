'use strict';

module.exports = function(app) {
	var eventos = require('../controllers/eventos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/eventos')
		.get(eventos.list)
		.post(apiAuth, users.requiresLogin, eventos.create);

	app.route('/eventos/:eventoId')
		.get(eventos.read)
		.put(apiAuth, users.requiresLogin, eventos.update)
		.delete(apiAuth, users.requiresLogin, eventos.delete);

	// Finish by binding the article middleware
	app.param('eventoId', eventos.getByID);
};