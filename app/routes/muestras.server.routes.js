'use strict';

module.exports = function(app) {
	var muestras = require('../controllers/muestras.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/muestras')
		.get(muestras.list)
		.post(apiAuth, users.requiresLogin, muestras.create);

	app.route('/muestras/:muestraId')
		.get(muestras.read)
		.put(apiAuth, users.requiresLogin, muestras.update)
		.delete(apiAuth, users.requiresLogin, muestras.delete);

	// Finish by binding the article middleware
	app.param('muestraId', muestras.getByID);
};