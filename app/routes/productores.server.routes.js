'use strict';

module.exports = function(app) {
	var productores = require('../controllers/productores.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/productores')
		.get(productores.list)
		.post(apiAuth, users.requiresLogin, productores.create);

	app.route('/productores/:productorId')
		.get(productores.read)
		.put(apiAuth, users.requiresLogin, productores.update)
		.delete(apiAuth, users.requiresLogin, productores.delete);

	// Finish by binding the article middleware
	app.param('productorId', productores.getByID);
};