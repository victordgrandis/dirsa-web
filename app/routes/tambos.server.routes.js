'use strict';

module.exports = function(app) {
	var tambos = require('../controllers/tambos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/tambos')
		.get(tambos.list)
		.post(apiAuth, users.requiresLogin, tambos.create);

	app.route('/tambos/:tamboId')
		.get(tambos.read)
		.put(apiAuth, users.requiresLogin, tambos.update)
		.delete(apiAuth, users.requiresLogin, tambos.delete);

	// Finish by binding the article middleware
	app.param('tamboId', tambos.getByID);
};