'use strict';

module.exports = function(app) {
	var toros = require('../controllers/toros.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/toros')
		.get(toros.list)
		.post(apiAuth, users.requiresLogin, toros.create);

	app.route('/toros/:toroId')
		.get(toros.read)
		.put(apiAuth, users.requiresLogin, toros.update)
		.delete(apiAuth, users.requiresLogin, toros.delete);

	// Finish by binding the article middleware
	app.param('toroId', toros.getByID);
};