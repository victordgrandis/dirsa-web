'use strict';

module.exports = function(app) {
	var tratamientos = require('../controllers/tratamientos.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/tratamientos')
		.get(tratamientos.list)
		.post(apiAuth, users.requiresLogin, tratamientos.create);

	app.route('/tratamientos/:tratamientoId')
		.get(tratamientos.read)
		.put(apiAuth, users.requiresLogin, tratamientos.update)
		.delete(apiAuth, users.requiresLogin, tratamientos.delete);

	// Finish by binding the article middleware
	app.param('tratamientoId', tratamientos.getByID);
};