'use strict';

module.exports = function(app) {
	var vacas = require('../controllers/vacas.server.controller');
	var users = require('../controllers/users.server.controller');
	var apiAuth = require('../controllers/api.authorization.server.controller');

	app.route('/vacas')
		.get(vacas.list)
		.post(apiAuth, users.requiresLogin, vacas.create);

	app.route('/vacas/:vacaId')
		.get(vacas.read)
		.put(apiAuth, users.requiresLogin, vacas.update)
		.delete(apiAuth, users.requiresLogin, vacas.delete);

	// Finish by binding the article middleware
	app.param('vacaId', vacas.getByID);
};