'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	EventoReproductivo = mongoose.model('EventoReproductivo');

/**
 * Globals
 */
var user, eventoReproductivo;

/**
 * Unit tests
 */
describe('Evento reproductivo Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			eventoReproductivo = new EventoReproductivo({
				// Add model fields
				// ...
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return eventoReproductivo.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		EventoReproductivo.remove().exec();
		User.remove().exec();

		done();
	});
});