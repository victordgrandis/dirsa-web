'use strict';

module.exports = {
	app: {
		title: 'dirsaWeb',
		description: 'Proyecto Final Comisión S11 Ingeniería en Sistemas de Información UTN FRLP',
		keywords: ''
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/hover/css/hover-min.css',
				'public/lib/toastr/toastr.min.css',
				'public/lib/bootstrap/dist/css/style.css',
				'public/lib/angular-material/angular-material.css',
				'public/lib/swiper/dist/css/swiper.css',
				'public/lib/angular-material-data-table/dist/md-data-table.min.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-aria/angular-aria.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-cookies/angular-cookies.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-messages/angular-messages.min.js',
				'public/lib/angular-material/angular-material.js',
				'public/lib/angular-sanitize/angular-sanitize.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/jquery/dist/jquery.min.js',
				'public/lib/moment/moment.js',
				'public/lib/angular-moment/angular-moment.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/ng-file-upload/ng-file-upload-shim.min.js',
				'public/lib/ng-file-upload/ng-file-upload.min.js',
				'public/lib/toastr/toastr.min.js',
				'public/lib/angular-filter/dist/angular-filter.min.js',
				'public/lib/angular-order-object-by/src/ng-order-object-by.js',
				'public/lib/angular-breadcrumb/dist/angular-breadcrumb.min.js',
				'public/lib/swiper/dist/js/swiper.min.js',
				'public/lib/angular-swiper/dist/angular-swiper.js',
				'public/lib/angular-material-data-table/dist/md-data-table.min.js',
				'public/lib/chart.js/dist/Chart.min.js',
				'public/lib/angular-chart.js/dist/angular-chart.js',
				'public/lib/ng-lodash/build/ng-lodash.min.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};