'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
    User = require('mongoose').model('User'),
    path = require('path'),
    config = require('./config');

/**
 * Module init function.
 */
module.exports = function() {
    // Serialize sessions
    passport.serializeUser(function(user, done) {
        var sessionUser = {
            _id: user.id,
            name: user.name,
            roles: user.roles
        };
        done(null, sessionUser);
    });

    // Deserialize sessions
    passport.deserializeUser(function(user, done) {
        User.findOne({
            _id: user._id
        }, '-salt -password', function(err, user) {
            done(err, user);
        });
    });

    // Initialize strategies
    config.getGlobbedFiles('./config/strategies/**/*.js').forEach(function(strategy) {
        require(path.resolve(strategy))();
    });
};