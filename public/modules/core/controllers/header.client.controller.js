'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', 'TamboServicio', '$location',
	function($scope, Authentication, Menus, TamboServicio, $location) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');
		$scope.barraLateral = false;
		$scope.toggle = true;
		
		if(TamboServicio.tambo){
			$scope.tambo = TamboServicio.tambo;
			$('#main-menu li').not('.ausente').removeClass('disabled');
			$('#sub-menu li').removeClass('disabled');
			$('.sub-menu').css('display','');
			$('#main-menu li a').not('ausente').removeClass('disabled');
		}
		
		if($location.path() !== '/' && $location.path() !== '/tambos'){
			$('.navbar').addClass('ticksBackground');
		} else {
			$('.navbar').removeClass('ticksBackground');
		}
		
		$scope.$on('$locationChangeSuccess', function(event){
				if($location.path() !== '/' && $location.path() !== '/tambos'){
					$('.navbar').addClass('ticksBackground');
				} else {
					$('.navbar').removeClass('ticksBackground');
				}
		});
		
		angular.element(document).ready(function(){
			$('.dropdown-submenu').hover(function(e){
				if($('#sub-menu li').is('.disabled')){
					$('.sub-menu').hide();
				}
			});
		});
		
		$scope.$on('seleccionarTambo', function(event, data){
			// Selección de Tambo
		 	$scope.tambo = data;
			$('#main-menu li').not('.ausente').removeClass('disabled');
			$('#sub-menu li').removeClass('disabled');
			$('.sub-menu').css('display','');
			$('#main-menu li a').not('ausente').removeClass('disabled');
		});

		$scope.salirTambo = function(){
			$scope.tambo = null;
			TamboServicio.salirTambo();
			$('#main-menu li, #main-menu li a').not('#tambos, #tambos a, #tambos li, #animal, #animal a, #toros, #toros li').addClass('disabled');
		};

		$scope.barraLateralToggle = function(){
			// Abrir la barra lateral
			if($scope.toggle && $scope.authentication.user){
				$('#page-wrapper').toggleClass('active');
				$('.navbar').toggleClass('active');
				$scope.barraLateral = !$scope.barraLateral;
			}
		};
		
		$scope.disableToggle = function(){
			$scope.toggle = !$scope.toggle;
		};
		
		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
		
		$scope.isAuthorized = function(role){
			if($scope.authentication.user){
				for(var i=0;i<$scope.authentication.user.roles.length;i++){
					if($scope.authentication.user.roles[i] === role) return true;
				}
				return false;
			}
    };
	}
]);