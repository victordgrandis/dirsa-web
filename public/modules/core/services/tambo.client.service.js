'use strict';

angular.module('core').service('TamboServicio', ['$rootScope', '$cookies',
  function($rootScope, $cookies){
    var tambo = this;
    this.tambo = $cookies.getObject('tambo');
    
    this.seleccionarTambo = function(tambo){
      this.tambo = tambo;
      $cookies.putObject('tambo', tambo);
      $rootScope.$broadcast('seleccionarTambo', this.tambo);
    };
    
    this.salirTambo = function(){
      this.tambo = null;
      $cookies.remove('tambo');
    };
}]);