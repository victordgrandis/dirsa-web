'use strict';

//Setting up route
angular.module('crias').config(['$stateProvider',
	function($stateProvider) {
		// Crias state routing
		$stateProvider.
		state('listCrias', {
			url: '/crias',
			templateUrl: 'modules/crias/views/list-crias.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado de Crías'
		  }
		}).
		state('createCria', {
			url: '/crias/create',
			templateUrl: 'modules/crias/views/create-cria.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Crear Cría'
		  }
		}).
		state('viewCria', {
			url: '/crias/:criaId',
			templateUrl: 'modules/crias/views/view-cria.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Ficha Cría'
		  }
		}).
		state('editCria', {
			url: '/crias/:criaId/edit',
			templateUrl: 'modules/crias/views/edit-cria.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Editar Cría'
		  }
		});
	}
]);