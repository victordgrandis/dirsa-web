'use strict';

// Crias controller
angular.module('crias').controller('CriasController', ['TamboServicio', '$scope', '$stateParams', '$location', 'Authentication', 'Crias',
	function(TamboServicio, $scope, $stateParams, $location, Authentication, Crias) {
			$scope.authentication = Authentication;
	  	$scope.currentPage = 1;
	  	$scope.pageSize = 10;
	  	$scope.offset = 0;
			$scope.orden = 'rp';
			$scope.sexo = 'Hembra';
			
			if(TamboServicio.tambo) $scope.tambo = TamboServicio.tambo._id;
			
	  	// Page changed handler
	  	$scope.pageChanged = function() {
	   	$scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
	  	};

		// Create new Cria
		$scope.create = function() {
			// Create new Cria object
			var cria = new Crias ({
				fechaNacimiento: this.fechaNacimiento,
				fechaIngreso: this.fechaIngreso,
				edad: this.edad,
				nombre: this.nombre,
				raza: this.raza,
				rp: this.rp,
				categoria: this.categoria,
				rpAbuela: this.rpAbuela,
				rpMadre: this.rpMadre,
				hbaPadre: this.hbaPadre,
				activa: this.activa,
				sexo: this.sexo,
				tambo: $scope.tambo
			});

			// Redirect after save
			cria.$save(function(response) {
				$location.path('crias/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Cria
		$scope.remove = function(cria) {
			if ( cria ) {
				cria.$remove();

				for (var i in $scope.crias) {
					if ($scope.crias [i] === cria) {
						$scope.crias.splice(i, 1);
					}
				}
			} else {
				$scope.cria.$remove(function() {
					$location.path('crias');
				});
			}
		};

		// Update existing Cria
		$scope.update = function() {
			var cria = $scope.cria;
			cria.fechaNacimiento = $scope.fechaNacimiento;
			cria.fechaIngreso = $scope.fechaIngreso;

			cria.$update(function() {
				$location.path('crias/' + cria._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		
		var append = function append(v) {
			if(v.fechaNacimiento) v.fechaNacimiento = new Date(v.fechaNacimiento);
			if(v.fechaIngreso) v.fechaIngreso = new Date(v.fechaIngreso);
		};
		
		// Find a list of Crias
		$scope.find = function() {
			$scope.crias = Crias.query();
		};

		// Find existing Cria
		$scope.findOne = function() {
			Crias.get({
				criaId: $stateParams.criaId
			}, append).$promise.then(function(cria){
				$scope.cria = cria;
				$scope.fechaNacimiento = cria.fechaNacimiento;
				$scope.fechaIngreso = cria.fechaIngreso;
			});
		};

		// Search for a cria
		$scope.criaSearch = function(product) {
			$location.path('crias/' + product._id);
		};
		
		$scope.redireccionar = function(id){
			$location.path('crias/'+id);
		};
		
		$scope.agregarCria = function(){
			$location.path('crias/create');
		};
	}
]);