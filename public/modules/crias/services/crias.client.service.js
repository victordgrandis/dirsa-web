'use strict';

// Crias service used to communicate Crias REST endpoints
angular.module('crias').factory('Crias', ['$resource',
	function($resource) {
		return $resource('crias/:criaId', { criaId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
