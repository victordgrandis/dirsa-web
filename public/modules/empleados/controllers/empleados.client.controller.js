'use strict';

angular.module('empleados').controller('EmpleadosController', ['$scope', '$stateParams', '$location', '$filter', 'Authentication', 'Empleados',
	function($scope, $stateParams, $location, $filter, Authentication, Empleados) {

		// Crear nuevo Empleado 
		$scope.create = function() {
				// Crear nuevo objeto
				var empleado = new Empleados({
						
				});

				// Redireccionar después de guardar
				empleado.$save(function(response) {
						$location.path('empleados/' + response._id);

						// Limpiar los campos
						$scope.name = '';
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Eliminar Empleado 
		$scope.remove = function(empleado) {
				if (empleado) {
						empleado.$remove();

						for (var i in $scope.empleados) {
								if ($scope.empleados[i] === empleado) {
										$scope.empleados.splice(i, 1);
								}
						}
				} else {
						$scope.empleado.$remove(function() {
								$location.path('empleados');
						});
				}
		};
		
		// Actualizar Empleado 
		$scope.update = function() {
				var empleado = $scope.empleado;

				empleado.$update(function() {
						$location.path('empleados/' + empleado._id);
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Find a list of Empleados
		$scope.find = function() {
        $scope.empleados = Empleados.query();
		};

		// Recuperar un Empleado 
		$scope.findOne = function() {
				$scope.empleado = Empleados.get({
						empleadoId: $stateParams.empleadoId
				});
		};

		// Busqueda de Empleado 
		$scope.empleadoSearch = function(empleado) {
				$location.path('empleados/' + empleado._id);
		};
	}
]);