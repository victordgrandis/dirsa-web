'use strict';

//Setting up route
angular.module('eventos-reproductivos').config(['$stateProvider',
	function($stateProvider) {
		// Eventos reproductivos state routing
		$stateProvider.
		state('cargaIndividualEventoReproductivo', {
			url: '/eventos-reproductivos/carga-individual',
			templateUrl: 'modules/eventos-reproductivos/views/carga-individual-evento-reproductivo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Carga Individual'
		  }
		}).
		state('cargaMasivoEventoReproductivo', {
			url: '/eventos-reproductivos/carga-masiva',
			templateUrl: 'modules/eventos-reproductivos/views/carga-masiva-evento-reproductivo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Carga Masiva'
		  }
		}).
		state('listEventosReproductivos', {
			url: '/eventos-reproductivos/listar',
			templateUrl: 'modules/eventos-reproductivos/views/list-eventos-reproductivos.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado de Eventos'
		  }
		}).
		state('ViewEventoReproductivo', {
			url: '/eventos-reproductivos/:eventoReproductivoId',
			templateUrl: 'modules/eventos-reproductivos/views/view-evento-reproductivo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Evento'
		  }
		}).
		state('EditEventoReproductivo', {
			url: '/eventos-reproductivos/:eventoReproductivoId/edit',
			templateUrl: 'modules/eventos-reproductivos/views/edit-evento-reproductivo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Editar Evento'
		  }
		});
	}
]);