'use strict';

// Configuring the Articles module
angular.module('eventos-reproductivos').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Eventos Reproductivos', 'eventos-reproductivos', 'dropdown', '/eventos-reproductivos(/create)?');
		Menus.addSubMenuItem('topbar', 'eventos-reproductivos', 'Listado Eventos Reproductivos', 'eventos-reproductivos');
		Menus.addSubMenuItem('topbar', 'eventos-reproductivos', 'Agregar Evento Reproductivo', 'eventos-reproductivos/create');
	}
]);