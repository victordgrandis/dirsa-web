'use strict';

angular.module('eventos-reproductivos').controller('EventosReproductivosController', ['lodash', '$scope', '$stateParams', '$location', '$state', '$filter', '$q', '$timeout', '$mdToast','$mdDialog', 'TamboServicio', 'Authentication', 'EventosReproductivos', 'Eventos', 'Empleados', 'Tratamientos', 'Vacas', 'TipoAnimal', 'ServicioToro', 'EventosReproductivosPorVaca',
    function(lodash, $scope, $stateParams, $location, $state, $filter, $q, $timeout, $mdToast, $mdDialog, TamboServicio, Authentication, EventosReproductivos, Eventos, Empleados, Tratamientos, Vacas, TipoAnimal, ServicioToro, EventosReproductivosPorVaca) {
        $scope.authentication = Authentication;
        $scope.eventos = Eventos.query();
        $scope.tratamientos = Tratamientos.query();
        $scope.empleados = Empleados.query();
        $scope.vacas = Vacas.query();
        $scope.currentPage = 1;
        $scope.pageListSize = 5;
        $scope.pageSize = 15;
        $scope.pageSizeVaca = 15;
        $scope.offset = 0;
        $scope.listaVacas = [];
        $scope.vaca = null;
        $scope.toro = null;
        $scope.botonRP = 'Seleccione RP';
        $scope.botonHBA = 'Seleccione HBA Toro';
        $scope.fecha = new Date();
        $scope.participante = null;
        $scope.orden = '-fecha';
        $scope.collapse = false;
        $scope.editar=false;
        
        if(TamboServicio.tambo) $scope.tambo = TamboServicio.tambo._id;

        // Cambio de página
        $scope.pageChanged = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
        };
        
        // Cambio de página
        $scope.pageListChanged = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageListSize;
        };

        // Cambio de página en Vaca
        $scope.pageChangedVaca = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageSizeVaca;
        };

        // Paso de Vaquillona a Vaca al segundo Servicio
        var tipoAnimal = function(v){
          var tipoAnimal;
          var eventosReproductivos = {};
          var cantidadServicios = 0;
          EventosReproductivosPorVaca.query({vaca:v}, function loadedEventos(eventosReproductivos) {
            eventosReproductivos.forEach(appendEvento);
            eventosReproductivos = eventosReproductivos;
          }).$promise.then(function(eventosReproductivos){
            for(var i = 0; i < eventosReproductivos.length; i++){
              if(eventosReproductivos[i].evento.codigo === 2){
                cantidadServicios += 1;
              }
            }
            if (cantidadServicios > 1){
              tipoAnimal = 'V';
              TipoAnimal.update(v, tipoAnimal);
            }
          });
        };
        
        
        var numeroServicio = function(){
          EventosReproductivosPorVaca.query({vaca:$scope.vaca}, function loadedEventos(eventosReproductivos) {
            eventosReproductivos.forEach(appendEvento);
            eventosReproductivos = eventosReproductivos;
          }).$promise.then(function(eventosReproductivos){
            var ev = _.orderBy(eventosReproductivos, 'fecha', 'desc');
            _.forEach(ev, function(e){
              if(e.evento.codigo === 3) return false;
              if(e.evento.codigo === 2){
                $scope.numero_servicio += 1;
              }
            });
            
          });
        };
        

        /*-------------------------------------*/
        /*           Funciones REST            */
        /*-------------------------------------*/

        // Crear nuevo Evento Reproductivo
        $scope.create = function() {
          var codigo_evento = this.evento.codigo;
          if(this.evento.codigo === 2){
            tipoAnimal($scope.vaca);
          } else if (this.evento.codigo === 3){
            $scope.toro = $scope.servicio.toro;
          }
            // Crear nuevo objeto
            var eventoReproductivo = new EventosReproductivos({
							evento: this.evento._id,
							vaca: $scope.vaca,
							fecha: this.fecha,
              participante: this.participante.nombre,
              tratamiento: this.tratamiento.numero,
              numero: $scope.numero_servicio,
              tambo: $scope.tambo,
              toro: $scope.toro
            });
            // Redireccionar después de guardar
            eventoReproductivo.$save(function(response) {
              if(codigo_evento === 2){
                ServicioToro.cargarServicio(response.toro, response._id);
              }
              if(codigo_evento === 3){
                ServicioToro.cargarPrenez(response.toro, response._id, $scope.servicio._id);
              }
              $location.path('eventos-reproductivos/listar');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });		
            
            $mdToast.show(
              $mdToast.simple()
                .textContent('Evento Cargado')
                .position('bottom right')
                .hideDelay(3000)
            );
        };
				
				// Crear n nuevos Eventos Reproductivos
				$scope.cargarMasivamente = function(){
          var codigo_evento = this.evento.codigo;
					// Crear n nuevos objetos
					for(var v in $scope.listaVacas){
            if(this.evento.codigo === 2){
              tipoAnimal($scope.listaVacas[v]);
            }
						var eventoReproductivo = new EventosReproductivos({
							evento: this.evento._id,
							vaca: $scope.listaVacas[v],
							fecha: this.fecha,
              participante: this.participante.nombre,
              tratamiento: this.tratamiento.numero,
              tambo: $scope.tambo,
              toro: $scope.toro
						});
            eventoReproductivo.$save(function(response){
              if(codigo_evento === 2){
                ServicioToro.cargarServicio(response.toro, response._id);
              }
            });
					}
          
          $location.path('eventos-reproductivos/listar');
          
          $mdToast.show(
            $mdToast.simple()
              .textContent('Eventos Cargados')
              .position('bottom right')
              .hideDelay(3000)
          );
				};
				
        // Eliminar Evento Reproductivo
        $scope.remove = function(eventoReproductivo) {
            if (eventoReproductivo) {
              if(eventoReproductivo.evento.codigo === 2){
                ServicioToro.eliminarServicio(eventoReproductivo.toro, eventoReproductivo._id);
              } else if (eventoReproductivo.evento.codigo === 3){
                ServicioToro.eliminarPrenez(eventoReproductivo.toro, eventoReproductivo._id);
              }
              eventoReproductivo.$remove();
              for (var i in $scope.eventosReproductivos) {
                  if ($scope.eventosReproductivos[i] === eventoReproductivo) {
                      $scope.eventosReproductivos.splice(i, 1);
                  }
              }
            } else {
              if($scope.eventoReproductivo.evento.codigo === 2){
                ServicioToro.eliminarServicio($scope.eventoReproductivo.toro, eventoReproductivo._id);
              } else if ($scope.eventoReproductivo.evento.codigo === 3){
                ServicioToro.eliminarPrenez($scope.eventoReproductivo.toro.toro, eventoReproductivo._id);
              }
              $scope.eventoReproductivo.$remove();
            }
            $state.reload();
            $mdToast.show(
              $mdToast.simple()
                .textContent('Evento Eliminado')
                .position('bottom right')
                .hideDelay(3000)
            );
        };

        // Actualizar Evento Reproductivo
        $scope.update = function() {
            var eventoReproductivo = $scope.eventoReproductivo;
            eventoReproductivo.fecha = $scope.fecha;
            eventoReproductivo.evento = $scope.eventoReproductivo.evento._id;
            if($scope.vaca) eventoReproductivo.vaca = $scope.vaca;
            if(eventoReproductivo.toro){
              ServicioToro.eliminarServicio($scope.toroAnterior);
              ServicioToro.cargarServicio(eventoReproductivo.toro);
            } 
            if($scope.eventoReproductivo.participante) eventoReproductivo.participante = $scope.eventoReproductivo.participante.nombre;
            if($scope.eventoReproductivo.tratamiento) eventoReproductivo.tratamiento = $scope.eventoReproductivo.tratamiento.numero;
            eventoReproductivo.$update(function() {
                $location.path('eventos-reproductivos/listar');
                $mdToast.show(
                  $mdToast.simple()
                    .textContent('Evento Actualizado')
                    .position('bottom right')
                    .hideDelay(3000)
                );
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        
        // Filtra datos correspondientes
        var appendEvento = function appendEvento(e) {
          e.evento = $filter('filter')($scope.eventos, { _id: e.evento })[0];
          e.tratamiento = $filter('filter')($scope.tratamientos, { numero: e.tratamiento })[0];
          e.participante = $filter('filter')($scope.empleados, { nombre: e.participante})[0];
          e.fecha = new Date(e.fecha);
        };

        // Find a list of EventosReproductivos
        $scope.find = function() {
          $timeout(function(){
            EventosReproductivos.query(function loadedEventos(eventosReproductivos) {
              eventosReproductivos.forEach(appendEvento);
              $scope.eventosReproductivos = eventosReproductivos;
            });
          });
        };

        // Recuperar un Evento Reproductivo
        $scope.findOne = function() {
          $timeout(function(){
            EventosReproductivos.get({
              eventoReproductivoId: $stateParams.eventoReproductivoId
            }, appendEvento).$promise.then(function(evento){
              $scope.eventoReproductivo = evento;
              $scope.fecha = evento.fecha;
            });
          });
        };
        
        // Busqueda de Evento Reproductivo
        $scope.eventoReproductivoSearch = function(eventoReproductivo) {
            $location.path('eventos-reproductivos/' + eventoReproductivo._id);
        };
        
      
        /*-------------------------------*/
        /*           Dialogs             */
        /*-------------------------------*/

        
        // Controlador del Dialog de seleccion de RP
        var DialogController = function($scope, $mdDialog) {
          $scope.selected = [];
          $scope.hide = function() {
            $mdDialog.hide();
          };
          
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          
          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };
        };
        
        // Controlador del Dialog de seleccion de RP
        var DialogMasivoController = function($scope, $mdDialog) {
          $scope.selected = [];
          $scope.hide = function() {
            $mdDialog.hide();
          };
          
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          
          $scope.seleccionar = function() {
            $mdDialog.hide($scope.selected);
          };
        };
        
        // Controlador del Dialog de detalle de evento
        var DialogDetalleController = function($scope, $mdDialog, eventoReproductivo) {
          $scope.eventoReproductivo = eventoReproductivo;
          $scope.hide = function() {
            $mdDialog.hide();
            $location.path('eventos-reproductivos/'+eventoReproductivo._id+'/edit');
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          
          $scope.answer = function() {
            $mdDialog.hide();
          };
        };
        
        // Botón de mostrar el Dialog de selección de RP en carga masiva
        $scope.masivoMostrarRP = function(ev) {
          $mdDialog.show({
            controller: DialogMasivoController,
            templateUrl: 'modules/eventos-reproductivos/directives/seleccionarRPMasivo.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            disableParentScroll: false
          })
          .then(function(answer) {
            _.forEach(answer, function(vaca){
              if ($scope.listaVacas.indexOf(vaca.rp) < 0) $scope.listaVacas.push(vaca.rp);
            })
          });
        };
        
        // Botón de mostrar el Dialog de selección de RP en carga individual
        $scope.individualMostrarRP = function(ev) {
            $mdDialog.show({
              controller: DialogController,
              templateUrl: 'modules/eventos-reproductivos/directives/seleccionarRP.html',
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose: true,
              disableParentScroll: false
              })
            .then(function(answer) {
              if($scope.eventoReproductivo){
                $scope.eventoReproductivo.vaca = answer.rp;
              } else {
                $scope.vaca = answer.rp;
              }
              $scope.botonRP = 'RP: ' + answer.rp;
              });
          };
          
          // Botón de mostrar el Dialog de selección de HBA en carga individual
          $scope.individualMostrarHBA = function(ev) {
              $mdDialog.show({
                controller: DialogController,
                templateUrl: 'modules/eventos-reproductivos/directives/seleccionarHBA.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                disableParentScroll: false,
                preserveScope: true,
                autoWrap: true,
                skipHide: true
                })
              .then(function(answer) {
                if($scope.eventoReproductivo){
                  $scope.toroAnterior = $scope.eventoReproductivo.toro;
                  $scope.eventoReproductivo.toro = answer.hbaToro;
                } else {
                  $scope.toro = answer.hbaToro;
                }
                $scope.botonHBA = 'HBA: ' + answer.hbaToro;
                });
            };
            
            $scope.mostrarDetalle = function(evento, eventoReproductivo){
              $mdDialog.show({
                controller: DialogDetalleController,
                templateUrl: 'modules/eventos-reproductivos/directives/detalleEvento.html',
                parent: angular.element(document.body),
                targetEvent: evento,
                locals: {
                  eventoReproductivo: eventoReproductivo
                },
                clickOutsideToClose: true,
                disableParentScroll: false
              });
            };
            
            /*---------------------------------*/
            /*           Servicios             */
            /*---------------------------------*/

            // Escuchar evento parto (codigo 3)
            $scope.escucharEvento = function(e){
              if($scope.vaca){
                if (e.codigo === 3) verificarServicios();
                if (e.codigo === 2){
                  $scope.numero_servicio = 1;
                  numeroServicio();
                }
              }
            };
            
            // Buscar servicio correspondiente al parto a ingresar
            var verificarServicios = function(){
              EventosReproductivosPorVaca.query({vaca:$scope.vaca}, function loadedEventos(eventosReproductivos) {
                eventosReproductivos.forEach(appendEvento);
                eventosReproductivos = eventosReproductivos;
              }).$promise.then(function(eventosReproductivos){
                for(var i = 0; i < eventosReproductivos.length; i++){
                  if(eventosReproductivos[i].evento.codigo === 2){
                    if($scope.servicio){
                      if(eventosReproductivos[i].fecha > $scope.servicio.fecha){
                        $scope.servicio = eventosReproductivos[i];
                      }
                    } else {
                      $scope.servicio = eventosReproductivos[i];
                    }
                  }
                }
              });
            };
          
          $scope.mostrarItem = function(item){
            $scope.detalleEvento = true;
            $scope.evento = item;
          };
          
          $scope.quitarRP = function(RP) {
            _.remove($scope.listaVacas, function(v){
              return v === RP
            })
          };
          
}]).config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('DD-MM-YYYY');
    };
    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
});