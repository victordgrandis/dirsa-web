'use strict';

angular.module('eventos').controller('EventosController', ['$scope', '$stateParams', '$location', '$filter', 'Authentication', 'Eventos',
	function($scope, $stateParams, $location, $filter, Authentication, Eventos) {

		// Crear nuevo Evento 
		$scope.create = function() {
				// Crear nuevo objeto
				var evento = new Eventos({
						
				});

				// Redireccionar después de guardar
				evento.$save(function(response) {
						$location.path('eventos/' + response._id);

						// Limpiar los campos
						$scope.name = '';
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Eliminar Evento 
		$scope.remove = function(evento) {
				if (evento) {
						evento.$remove();

						for (var i in $scope.eventos) {
								if ($scope.eventos[i] === evento) {
										$scope.eventos.splice(i, 1);
								}
						}
				} else {
						$scope.evento.$remove(function() {
								$location.path('eventos');
						});
				}
		};
		
		// Actualizar Evento 
		$scope.update = function() {
				var evento = $scope.evento;

				evento.$update(function() {
						$location.path('eventos/' + evento._id);
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Find a list of Eventos
		$scope.find = function() {
        $scope.eventos = Eventos.query();
		};

		// Recuperar un Evento 
		$scope.findOne = function() {
				$scope.evento = Eventos.get({
						eventoId: $stateParams.eventoId
				});
		};

		// Busqueda de Evento 
		$scope.eventoSearch = function(evento) {
				$location.path('eventos/' + evento._id);
		};
	}
]);