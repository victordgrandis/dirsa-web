'use strict';

angular.module('tratamientos').controller('TratamientosController', ['$scope', '$stateParams', '$location', '$filter', 'Authentication', 'Tratamientos',
	function($scope, $stateParams, $location, $filter, Authentication, Tratamientos) {

		// Crear nuevo Tratamiento 
		$scope.create = function() {
				// Crear nuevo objeto
				var tratamiento = new Tratamientos({
						
				});

				// Redireccionar después de guardar
				tratamiento.$save(function(response) {
						$location.path('tratamientos/' + response._id);

						// Limpiar los campos
						$scope.name = '';
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Eliminar Tratamiento 
		$scope.remove = function(tratamiento) {
				if (tratamiento) {
						tratamiento.$remove();

						for (var i in $scope.tratamientos) {
								if ($scope.tratamientos[i] === tratamiento) {
										$scope.tratamientos.splice(i, 1);
								}
						}
				} else {
						$scope.tratamiento.$remove(function() {
								$location.path('tratamientos');
						});
				}
		};
		
		// Actualizar Tratamiento 
		$scope.update = function() {
				var tratamiento = $scope.tratamiento;

				tratamiento.$update(function() {
						$location.path('tratamientos/' + tratamiento._id);
				}, function(errorResponse) {
						$scope.error = errorResponse.data.mesage;
				});
		};

		// Find a list of Tratamientos
		$scope.find = function() {
        $scope.tratamientos = Tratamientos.query();
		};

		// Recuperar un Tratamiento 
		$scope.findOne = function() {
				$scope.tratamiento = Tratamientos.get({
						tratamientoId: $stateParams.tratamientoId
				});
		};

		// Busqueda de Tratamiento 
		$scope.tratamientoSearch = function(tratamiento) {
				$location.path('tratamientos/' + tratamiento._id);
		};
	}
]);