'use strict';

// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('eventos-reproductivos');
ApplicationConfiguration.registerModule('eventos');
ApplicationConfiguration.registerModule('tratamientos');
