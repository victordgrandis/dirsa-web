'use strict';

// EventosReproductivos service usado para comunicar EventosReproductivos REST
angular.module('eventos-reproductivos').factory('EventosReproductivos', ['$resource',
    function($resource) {
        return $resource('eventos-reproductivos/:eventoReproductivoId', {
            eventoReproductivoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]).factory('EventosReproductivosPorVaca', ['$resource',
    function($resource){
      return $resource('eventos-reproductivos' ,{
        query: {
                method: 'GET',
                isArray: true
        },
        update: {
          method: 'PUT'
        }
      });
    }
]);
