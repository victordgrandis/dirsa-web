'use strict';

//Eventos service used to communicate Eventos REST endpoints
angular.module('eventos-reproductivos').factory('Eventos', ['$resource',
    function($resource) {
        return $resource('/eventos/:eventoId', {
            eventoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);