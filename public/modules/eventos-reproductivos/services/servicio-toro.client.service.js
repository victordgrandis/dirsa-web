'use strict';

angular.module('eventos-reproductivos').service('ServicioToro', ['ToroPorHBA', 'Toros',
  function(ToroPorHBA, Toros){
    
    // Actualizar toro
    this.cargarServicio = function(HBA, evento_id) {
      ToroPorHBA.query({hbaToro:HBA}).$promise.then(function(toros){
        Toros.get({toroId: toros[0]._id}).$promise.then(function(toro){
          toro.servicios.push(evento_id);
          toro.$update();
        });
      });
    };
    // Actualizar toro
    this.eliminarServicio = function(HBA, evento_id) {
      ToroPorHBA.query({hbaToro:HBA}).$promise.then(function(toros){
        Toros.get({toroId: toros[0]._id}).$promise.then(function(toro){  
         _.remove(toro.servicios, function(serv){
           return serv ===  evento_id;
         });
          toro.$update();
        });
      });
    };
    // Actualizar toro
    this.cargarPrenez = function(HBA, evento_id, servicio_id) {
      ToroPorHBA.query({hbaToro:HBA}).$promise.then(function(toros){
        Toros.get({toroId: toros[0]._id}).$promise.then(function(toro){
          var pren =  {'evento': evento_id, 'servicio': servicio_id};
          toro.preñeces.push(pren);
          toro.$update();
        });
      });
    };
    // Actualizar toro
    this.eliminarPrenez = function(HBA, evento_id) {
      ToroPorHBA.query({hbaToro:HBA}).$promise.then(function(toros){
        Toros.get({toroId: toros[0]._id}).$promise.then(function(toro){  
          _.remove(toro.preñeces, function(pren){
            return pren.evento === evento_id;
          });
          toro.$update();
        });
      });
    };
        
}]);