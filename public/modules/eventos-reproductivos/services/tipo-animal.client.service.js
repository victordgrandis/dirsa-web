'use strict';

angular.module('eventos-reproductivos').service('TipoAnimal', ['VacaPorRP', 'Vacas',
  function(VacaPorRP, Vacas){
    
    // Actualizar vaca
    this.update = function(RP, tipo) {
        VacaPorRP.query({rp:RP}).$promise.then(function(vacas){
          Vacas.get({vacaId: vacas[0]._id}).$promise.then(function(vaca){  
            vaca.tipoAnimal = tipo;
            vaca.$update();
          });
        });
    };
}]);