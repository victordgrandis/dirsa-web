'use strict';

//Tratamientos service used to communicate Tratamientos REST endpoints
angular.module('tratamientos').factory('Tratamientos', ['$resource',
    function($resource) {
        return $resource('/tratamientos/:tratamientoId', {
            eventoId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);