'use strict';

// Configuring the Articles module
angular.module('productores').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Productores', 'productores', 'dropdown', '/productores(/create)?');
		Menus.addSubMenuItem('topbar', 'productores', 'Listado Productores', 'productores');
		Menus.addSubMenuItem('topbar', 'productores', 'Agregar Productor', 'productores/create');
	}
]);