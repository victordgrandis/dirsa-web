'use strict';

//Setting up route
angular.module('productores').config(['$stateProvider',
	function($stateProvider) {
		// Productores state routing
		$stateProvider.
		state('listProductores', {
			url: '/productores',
			templateUrl: 'modules/productores/views/list-productores.client.view.html'
		}).
		state('createProductor', {
			url: '/productores/create',
			templateUrl: 'modules/productores/views/create-productor.client.view.html'
		}).
		state('viewProductor', {
			url: '/productores/:productorId',
			templateUrl: 'modules/productores/views/view-productor.client.view.html'
		}).
		state('editProductor', {
			url: '/productores/:productorId/edit',
			templateUrl: 'modules/productores/views/edit-productor.client.view.html'
		});
	}
]);