'use strict';

// Productores controller
angular.module('productores').controller('ProductoresController', ['$scope', '$stateParams', '$location', 'Authentication', 'Productores',
	function($scope, $stateParams, $location, Authentication, Productores) {
		$scope.authentication = Authentication;
	  	$scope.currentPage = 1;
	  	$scope.pageSize = 10;
	  	$scope.offset = 0;

	  	// Page changed handler
	  	$scope.pageChanged = function() {
	   	$scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
	  	};

		// Create new Productor
		$scope.create = function() {
			// Create new Productor object
			var productor = new Productores ({
				idProductor: this.idProductor,
				direccion: this.direccion,
				nombre: this.nombre,
				provincia: this.provincia,
				telefono: this.telefono
			});

			// Redirect after save
			productor.$save(function(response) {
				$location.path('productores/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Productor
		$scope.remove = function(productor) {
			if ( productor ) {
				productor.$remove();

				for (var i in $scope.productores) {
					if ($scope.productores [i] === productor) {
						$scope.productores.splice(i, 1);
					}
				}
			} else {
				$scope.productor.$remove(function() {
					$location.path('productores');
				});
			}
		};

		// Update existing Productor
		$scope.update = function() {
			var productor = $scope.productor;

			productor.$update(function() {
				$location.path('productores/' + productor._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Productores
		$scope.find = function() {
			$scope.productores = Productores.query();
		};

		// Find existing Productor
		$scope.findOne = function() {
			$scope.productor = Productores.get({
				productorId: $stateParams.productorId
			});
		};

		// Search for a productor
		$scope.productorSearch = function(product) {
			$location.path('productores/' + product._id);
		};
	}
]);