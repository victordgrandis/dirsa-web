'use strict';

angular.module('productores').factory('Productores', ['$resource',
		function($resource) {
			return $resource('productores/:productorId', { productorId: '@_id'
			}, {
				update: {
					method: 'PUT'
				}
			});
		}
	]);