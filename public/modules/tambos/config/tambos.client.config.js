'use strict';

// Configuring the Articles module
angular.module('tambos').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Tambos', 'tambos', 'dropdown', '/tambos(/create)?');
		Menus.addSubMenuItem('topbar', 'tambos', 'Listado Tambos', 'tambos');
		Menus.addSubMenuItem('topbar', 'tambos', 'Agregar Tambo', 'tambos/create');
	}
]);