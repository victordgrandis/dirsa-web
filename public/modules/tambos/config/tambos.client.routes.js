'use strict';

//Setting up route
angular.module('tambos').config(['$stateProvider',
	function($stateProvider) {
		// Tambos state routing
		$stateProvider.
		state('listTambos', {
			url: '/tambos',
			templateUrl: 'modules/tambos/views/list-tambos.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado de Tambos'
		  }
		}).
		state('createTambo', {
			url: '/tambos/create',
			templateUrl: 'modules/tambos/views/create-tambo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Crear Nuevo Tambo'
		  }
		}).
		state('viewTambo', {
			url: '/tambos/:tamboId',
			templateUrl: 'modules/tambos/views/view-tambo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Tambo'
		  }
		}).
		state('editTambo', {
			url: '/tambos/:tamboId/edit',
			templateUrl: 'modules/tambos/views/edit-tambo.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Editar Tambo'
		  }
		});
	}
]);