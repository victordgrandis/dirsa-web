'use strict';

// Tambos controller
angular.module('tambos').controller('TambosController', ['$scope', '$stateParams', '$location', 'Authentication', 'Tambos', 'TamboServicio','Productores', '$filter',
	function($scope, $stateParams, $location, Authentication, Tambos, TamboServicio, Productores, $filter) {
		$scope.authentication = Authentication;
		$scope.productores = Productores.query();
		$scope.currentPage = 1;
		$scope.pageSize = 10;
		$scope.offset = 0;

		// Controlador de cambio de página
		$scope.pageChanged = function() {
			$scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
		};

		// Crear un nuevo tambo
		$scope.create = function() {
			var tambo = new Tambos ({
				productor: this.productor._id,
				nombre: this.nombre,
				direccion: this.direccion,
				fechaAlta: this.fechaAlta,
				idTambo: this.idTambo,
				idAcha: this.idAcha
			});
			tambo.$save(function(response) {
				$location.path('/');
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Eliminar tambo
		$scope.remove = function(tambo) {
			if ( tambo ) {
				tambo.$remove();

				for (var i in $scope.tambos) {
					if ($scope.tambos [i] === tambo) {
						$scope.tambos.splice(i, 1);
					}
				}
			} else {
				$scope.tambo.$remove(function() {
					$location.path('tambos');
				});
			}
		};

		// Actualizar tambo
		$scope.update = function() {
			var tambo = $scope.tambo;
			tambo.productor = tambo.productor._id;
			tambo.fechaAlta = $scope.fechaAlta;
			tambo.$update(function() {
				$location.path('tambos/' + tambo._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		var appendProductor = function appendProductor(p) {
			p.productor = $filter('filter')($scope.productores, {_id: p.productor})[0];
			p.fechaAlta = new Date(p.fechaAlta);
		};

		// Lista tambos
		$scope.find = function() {
			Tambos.query(function loadedTambos(tambos) {
				tambos.forEach(appendProductor);
				$scope.tambos = tambos;
			});
		};

		// Devuelve un tambo
		$scope.findOne = function() {
			$scope.tambo = Tambos.get({
				tamboId: $stateParams.tamboId
			}, appendProductor);
			$scope.fechaAlta = $scope.tambo.fechaAlta;
		};
		
		$scope.filtroUsuario = function(tambo){
				if($scope.authentication.user.tambos.indexOf(tambo._id) !== -1){
					return tambo;
				}
		};

		// Busca un tambo
		$scope.tamboSearch = function(tambo) {
			$location.path('tambos/' + tambo._id);
		};
		
		$scope.seleccionarTambo = function(tambo){
			TamboServicio.seleccionarTambo(tambo);
			$location.path('/');
		};
	}
]);