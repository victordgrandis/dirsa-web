'use strict';

//Tambos service used to communicate Tambos REST endpoints
angular.module('tambos').factory('Tambos', ['$resource',
	function($resource) {
		return $resource('tambos/:tamboId', { tamboId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
