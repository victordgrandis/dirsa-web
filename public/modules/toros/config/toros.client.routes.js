'use strict';

//Setting up route
angular.module('toros').config(['$stateProvider',
	function($stateProvider) {
		// Toros state routing
		$stateProvider.
		state('listToros', {
			url: '/toros',
			templateUrl: 'modules/toros/views/list-toros.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado Toros'
		  }
		}).
		state('createToro', {
			url: '/toros/create',
			templateUrl: 'modules/toros/views/create-toro.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Nuevo Toro'
		  }
		}).
		state('viewToro', {
			url: '/toros/:toroId',
			templateUrl: 'modules/toros/views/view-toro.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Estadísticas Toro'
		  }
		}).
		state('editToro', {
			url: '/toros/:toroId/edit',
			templateUrl: 'modules/toros/views/edit-toro.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Editar Toro'
		  }
		});
	}
]);