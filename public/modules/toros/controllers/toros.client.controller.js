'use strict';

// Toros controller
angular.module('toros').controller('TorosController', ['$scope', '$stateParams', '$location', '$filter', '$mdDialog',  'Authentication', 'Toros', 'EventosReproductivos', 'Eventos',
	function($scope, $stateParams, $location, $filter, $mdDialog, Authentication, Toros, EventosReproductivos, Eventos) {
		$scope.authentication = Authentication;
	  	$scope.currentPage = 1;
	  	$scope.pageSize = 10;
			$scope.eventos = Eventos.query();
			$scope.eventosReproductivos = EventosReproductivos.query();
	  	$scope.offset = 0;
			$scope.orden = 'hbaToro';
			$scope.tipoServicios = ['No Sabe','A Campo','A Corral','Ins. Artificial','Trans. Embriones'];
			$scope.fecha_desde = new Date(moment().subtract(1,'month'));
			$scope.fecha_hasta = new Date();
			$scope.servicios = [[],[],[],[],[],[],[],[],[]];
			$scope.preñeces = [[],[],[],[],[],[],[],[],[]];

	  	// Page changed handler
	  	$scope.pageChanged = function() {
	   	$scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
	  	};

		// Create new Toro
		$scope.create = function() {
			// Create new Toro object
			var toro = new Toros ({
				fechaNacimiento: this.fechaNacimiento,
				fechaIngreso: this.fechaIngreso,
				edad: this.edad,
				nombre: this.nombre,
				raza: this.raza,
				apodo: this.apodo,
				hbaToro: this.hbaToro,
				rp: this.rp,
				tipoServicio: this.tipoServicio
			});

			// Guardar y redireccionar
			toro.$save(function(response) {
				$location.path('toros');
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Eliminar toro
		$scope.remove = function(toro) {
			if ( toro ) {
				toro.$remove();

				for (var i in $scope.toros) {
					if ($scope.toros [i] === toro) {
						$scope.toros.splice(i, 1);
					}
				}
			} else {
				$scope.toro.$remove(function() {
					$location.path('toros');
				});
			}
		};

		// Actualizar toro
		$scope.update = function() {
			var toro = $scope.toro;
			toro.fechaNacimiento = $scope.fechaNacimiento;
			toro.fechaIngreso = $scope.fechaIngreso;
			toro.$update(function() {
				$location.path('toros');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		
		// Controlador del Dialog de detalle de evento
		var DialogDetalleController = function($scope, $mdDialog, toro) {
			$scope.toro = toro;
			$scope.hide = function() {
				$mdDialog.hide();
				$location.path('toros/'+toro._id+'/edit');
			};
			$scope.hide2 = function() {
				$scope.seleccionado = toro;
				$mdDialog.hide();
				$location.path('toros/'+toro._id);
			};
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			
			$scope.answer = function(answer) {
				$mdDialog.hide(answer);
			};
		};
		
		
		// Filtra datos correspondientes
		var appendEvento = function appendEvento(e) {
			e.evento = $filter('filter')($scope.eventos, { _id: e.evento })[0];
		};

		
		$scope.calcularEstadisticas = function(){
			$scope.verGraficos = true;
			$scope.data_servicios = [];
			$scope.data_preneces = [];
			EventosReproductivos.query(function loadedEventos(eventosReproductivos) {
				eventosReproductivos.forEach(appendEvento);
				eventosReproductivos = eventosReproductivos;
			}).$promise.then(function(eventosReproductivos){
				var eventos = _.filter(eventosReproductivos, function(e){
					return moment(e.fecha).isAfter($scope.fecha_desde) && moment(e.fecha).isBefore(moment($scope.fecha_hasta).endOf('day'));
				});
				var servicios = _.filter(eventos, function(e){
					return e.evento.codigo === 2 && _.includes($scope.toro.servicios, e._id);
				});
				_.forEach($scope.toro.preñeces, function(p){
					if(p !== null && typeof p.servicio !== 'undefined') $scope.preñeces[p.servicio.numero - 1].push(p);
				});
				_.forEach(servicios, function(serv){
					$scope.servicios[serv.numero - 1].push(serv);
				});
				_.forEach($scope.servicios, function(serv, index){
					$scope.data_servicios[index] = serv.length;
				});
				_.forEach($scope.preñeces, function(pren, index){
					$scope.data_preneces[index] = pren.length;
				});
			});
		};
		
		$scope.mostrarDetalle = function(evento, toro){
			$mdDialog.show({
				controller: DialogDetalleController,
				templateUrl: 'modules/toros/directives/detalleToro.html',
				parent: angular.element(document.body),
				targetEvent: evento,
				locals: {
					toro: toro
				},
				clickOutsideToClose: true
			});
		};

		// Retorna listado de toros
		$scope.find = function() {
			$scope.toros = Toros.query();
		};
		
		var append = function append(t) {
			if(t.fechaNacimiento) t.fechaNacimiento = new Date(t.fechaNacimiento);
			if(t.fechaIngreso) t.fechaIngreso = new Date(t.fechaIngreso);
		};
			
		// Devuelve un toro
		$scope.findOne = function() {
			Toros.get({
				toroId: $stateParams.toroId
			}, append).$promise.then(function(toro){
				$scope.toro = toro;
				$scope.fechaNacimiento = toro.fechaNacimiento;
				$scope.fechaIngreso = toro.fechaIngreso;
				_.forEach($scope.toro.preñeces, function(p){
					if(p !== null) p.servicio = $filter('filter')($scope.eventosReproductivos, {_id: p.servicio})[0];
				});
			});
		};

		// Búsqueda
		$scope.toroSearch = function(product) {
			$location.path('toros/' + product._id);
		};
		
		$scope.redireccionar = function(){
			$location.path('toros/create');
		};
		
		$scope.labels = ['Primero', 'Segundo', 'Tercero','Cuarto','Quinto','Sexto','Septimo','Octavo','Noveno'];
	}
]);