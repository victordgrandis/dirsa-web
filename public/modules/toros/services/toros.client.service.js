'use strict';

// Vacas service used to communicate Vacas REST endpoints
angular.module('toros').factory('Toros', ['$resource',
	function($resource) {
		return $resource('toros/:toroId', { toroId: '@_id'
	}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('ToroPorHBA', ['$resource',
    function($resource){
      return $resource('toros' ,{
        query: {
            method: 'GET',
						isArray: false
        }
      });
    }
]);