'use strict';

// Vacas controller
angular.module('users').controller('UsersController', ['$scope', '$stateParams', '$http', '$location', '$filter', '$timeout', 'Authentication', 'Users', 'Tambos',
    function($scope, $stateParams, $http, $location, $filter, $timeout, Authentication, Users, Tambos) {
        $scope.authentication = Authentication;
        $scope.tambos = Tambos.query();
        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.offset = 0;
        $scope.orden = 'displayName';
        $scope.selectedItem = null;
        $scope.searchText = null;
        $scope.roles = [];
        $scope.userTambos = [];


        /*-------------------------------------*/
        /*           Funciones REST            */
        /*-------------------------------------*/
        
        
        $scope.update = function(isValid) {
          if (isValid) {
            $scope.success = $scope.error = null;
            var user = $scope.user;
            user.tambos = [];
            for(var t in $scope.userTambos){
              user.tambos.push($scope.userTambos[t]._id); 
            }
            user.$update(function(response) {
              $scope.success = true;
        			/*$http.post('/users/password', $scope.passwordDetails).success(function(response) {
        				// If successful show success message and clear form
        				$scope.success = true;
        				$scope.passwordDetails = null;
        			}).error(function(response) {
        				$scope.error = response.message;
        			});*/
              $location.path('/users');
            }, function(response) {
              $scope.error = response.data.message;
            });
          } else {
            $scope.submitted = true;
          }
        };
        
        $scope.create = function() {
          var user = new Users({
            firstName: this.firstName,
            lastName: this.lastName,
            displayName: this.displayName,
            email: this.email,
            username: this.username,
            password: this.password,
            roles: $scope.roles
          });
          user.tambos = [];
          for(var t in $scope.userTambos){
            user.tambos.push($scope.userTambos[t]._id); 
          }
          user.$save(function(response) {
            $scope.success = true;
            $location.path('/users');
          }, function(response) {
            $scope.error = response.data.message;
          });
        };
        
        // Eliminar usuario
        $scope.remove = function(user) {
            if (user) {
                user.$remove();
                for (var i in $scope.users) {
                    if ($scope.users[i] === user) {
                        $scope.users.splice(i, 1);
                    }
                }
            } else {
                $scope.user.$remove(function() {
                    $location.path('users');
                });
            }
        };

        // Filtra datos correspondientes
        var appendUser = function appendUser(u) {
          $timeout(function(){
            for(var t in u.tambos){
              u.tambos[t] = ($filter('filter')($scope.tambos, { _id: u.tambos[t] })[0]);
            }
            $scope.userTambos = u.tambos;
          });
        };
        
        // Retorna listado de usuarios
        $scope.find = function() {
            Users.query(function loadedUsers(users) {
              users.forEach(appendUser);
              $scope.users = users;
              $scope.noOfPages = Math.ceil($scope.users.length/$scope.pageSize);
            });
        };
        
        // Recuperar un Usuario
        $scope.findOne = function() {
            Users.get({ userId: $stateParams.userId }, appendUser).$promise.then(function(user){
              $scope.user = user;
            });
        };
        $scope.tambosQuery = function(query) {
            var results = query ? $scope.tambos.filter(filterTambos(query)) : [];
            return results;
        };

        //  Filter del tambosQuery
        function filterTambos(query) {
          return function filterFn(tambo) {
            return (tambo.nombre.includes(query));
          };
        }
        
        $scope.nuevoUsuario = function(){
          $location.path('/signup');
        };
        
        $scope.editarUsuario = function(user){
          $location.path('/settings/profile/'+user._id);
        };
}]);