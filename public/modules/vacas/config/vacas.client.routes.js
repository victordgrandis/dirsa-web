'use strict';

//Setting up route
angular.module('vacas').config(['$stateProvider',
	function($stateProvider) {
		// Vacas state routing
		$stateProvider.
		state('listVacas', {
			url: '/vacas',
			templateUrl: 'modules/vacas/views/list-vacas.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado Vacas'
		  }
		}).
		state('listVaquillonas', {
			url: '/vaquillonas',
			templateUrl: 'modules/vacas/views/list-vaquillonas.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Listado Vaquillonas'
		  }
		}).
		state('createVaca', {
			url: '/vacas/create',
			templateUrl: 'modules/vacas/views/create-vaca.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Nueva Vaca'
		  }
		}).
		state('viewVaca', {
			url: '/vacas/:vacaId',
			templateUrl: 'modules/vacas/views/view-vaca.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Ficha Vaca'
		  }
		}).
		state('editVaca', {
			url: '/vacas/:vacaId/edit',
			templateUrl: 'modules/vacas/views/edit-vaca.client.view.html',
		  ncyBreadcrumb: {
		    label: 'Editar Vaca'
		  }
		});
	}
]);