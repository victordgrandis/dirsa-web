'use strict';

// Vacas controller
angular.module('vacas').controller('VacasController', ['TamboServicio', '$scope', '$stateParams', '$location', '$q', '$filter', '$timeout', '$mdDialog', 'Authentication', 'Vacas',
    function(TamboServicio, $scope, $stateParams, $location, $q, $filter, $timeout, $mdDialog, Authentication, Vacas) {
        $scope.authentication = Authentication;
        $scope.currentPage = 1;
        $scope.pageSize = 15;
        $scope.pageSizeDialog = 8;
        $scope.offset = 0;
        $scope.detalleEvento = false;
        $scope.evento = null;
        $scope.orden = 'rp';
        $scope.swiper= {};
        
        if(TamboServicio.tambo) $scope.tambo = TamboServicio.tambo._id;

        // Paginación
        $scope.pageChanged = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageSize;
        };
        
        // Cambio de página en Dialog
        $scope.pageChangedDialog = function() {
            $scope.offset = ($scope.currentPage - 1) * $scope.pageSizeDialog;
        };
        
        $scope.filter = function() {
             $timeout(function() {
                 $scope.noOfPages = Math.ceil($scope.filtered.length/$scope.pageSize);
             }, 10);
         };
         
        // Crea nueva Vaca
        $scope.create = function() {
            var vaca = new Vacas({
        				fechaIngresoSistema: this.fechaIngresoSistema,
        				fechaIngresoTambo: this.fechaIngresoTambo,
                edad: this.edad,
                fechaNacimiento: this.fechaNacimiento,
                nombre: this.nombre,
                raza: this.raza,
                rp: this.rp,
                rc: this.rc,
                rpMadre: this.rpMadre,
                hbaPadre: this.hbaPadre,
                fechaInicioLactancia: this.fechaInicioLactancia,
                numeroLactancia: this.numeroLactancia,
                cantidadServicios: this.cantidadServicios,
                categoria: this.categoria,
                caravana: this.caravana,
                diasAlParto: this.diasAlParto,
                diasPrenez: this.diasPrenez,
                diasSeca: this.diasSeca,
                noRetorno: this.noRetorno,
                loteActual: this.loteActual,
                loteAnterior: this.loteAnterior,
                fechaSecado: this.fechaSecado,
                proximoSecado: this.proximoSecado,
                proximoParto: this.proximoParto,
                indiceFertilidad: this.indiceFertilidad,
                ipp: this.ipp,
                ippp: this.ippp,
                ipc: this.ipc,
                activa: this.activa,
                tambo: $scope.tambo
            });

            // Guardar y redireccionar
            vaca.$save(function(response) {
                $location.path('vacas/' + response._id);
                $scope.name = '';
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        // Eliminar vaca
        $scope.remove = function(vaca) {
            if (vaca) {
                vaca.$remove();
                for (var i in $scope.vacas) {
                    if ($scope.vacas[i] === vaca) {
                        $scope.vacas.splice(i, 1);
                    }
                }
            } else {
                $scope.vaca.$remove(function() {
                    $location.path('vacas');
                });
            }
        };

        // Actualizar vaca
        $scope.update = function() {
            var vaca = $scope.vaca;
            vaca.fechaNacimiento = $scope.fechaNacimiento;
            vaca.fechaSecado = $scope.fechaSecado;
            vaca.fechaIngresoTambo = $scope.fechaIngresoTambo;
            vaca.fechaIngresoSistema = $scope.fechaIngresoSistema;
            vaca.fechaInicioLactancia = $scope.fechaInicioLactancia;
            vaca.proximoSecado = $scope.proximoSecado;
            vaca.proximoParto = $scope.proximoParto;
            vaca.$update(function() {
                $location.path('vacas/' + vaca._id);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
        
        var append = function append(v) {
          if(v.fechaNacimiento) v.fechaNacimiento = new Date(v.fechaNacimiento);
          if(v.fechaSecado) v.fechaSecado = new Date(v.fechaSecado);
          if(v.fechaIngresoTambo) v.fechaIngresoTambo = new Date(v.fechaIngresoTambo);
          if(v.fechaIngresoSistema) v.fechaIngresoSistema = new Date(v.fechaIngresoSistema);
          if(v.fechaInicioLactancia) v.fechaInicioLactancia = new Date(v.fechaInicioLactancia);
          if(v.proximoSecado) v.proximoSecado = new Date(v.proximoSecado);
          if(v.proximoParto) v.proximoParto = new Date(v.proximoParto);
        };

        // Retorna listado de vacas
        $scope.find = function() {
            $scope.vacas = Vacas.query();
            $scope.noOfPages = Math.ceil($scope.vacas.length/$scope.pageSize);
        };

        // Devuelve una vaca
        $scope.findOne = function() {
            Vacas.get({
                vacaId: $stateParams.vacaId
            }, append).$promise.then(function(vaca){
              $scope.vaca = vaca;
              $scope.fechaNacimiento = vaca.fechaNacimiento;
              $scope.fechaSecado = vaca.fechaSecado;
              $scope.fechaIngresoTambo = vaca.fechaIngresoTambo;
              $scope.fechaIngresoSistema = vaca.fechaIngresoSistema;
              $scope.fechaInicioLactancia = vaca.fechaInicioLactancia;
              $scope.proximoSecado = vaca.proximoSecado;
              $scope.proximoParto = vaca.proximoParto;
            });
        };

        // Búsqueda
        $scope.vacaSearch = function(vaca) {
            $location.path('vacas/' + vaca._id);
        };
        
        $scope.redireccionar = function(id){
          $location.path('vacas/'+id);
        };
        
        $scope.agregarVaca = function(){
          $location.path('vacas/create');
        };
        
        // Controlador del Dialog de detalle de evento
        var DialogEventoController = function($scope, $mdDialog, rp) {
          $scope.rp = rp;
          $scope.hide = function() {
            $mdDialog.hide();
          };
          $scope.cancel = function() {
            $mdDialog.cancel();
          };
          
          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };
        };
        
        $scope.agregarEvento = function(evento, rp){
          $mdDialog.show({
            controller: DialogEventoController,
            templateUrl: 'modules/vacas/directives/agregarEvento.html',
            parent: angular.element(document.body),
            targetEvent: evento,
            locals: {
              rp: rp
            },
            clickOutsideToClose: true,
            disableParentScroll: false
          });
        };

        // Swiper de crear vaca
        $scope.siguienteTarjeta = function(){
          $scope.swiper.slideNext();
        };
        
        $scope.anteriorTarjeta = function(){
          $scope.swiper.slidePrev();
        };

    }
]).config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('DD-MM-YYYY');
    };
    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
});
