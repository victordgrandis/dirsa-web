'use strict';

angular.module('vacas').directive('vaca-eventos',function(){
	return{
		restrict: 'E',
		templateUrl: 'modules/vacas/directives/listaEventos.html'
	};
});