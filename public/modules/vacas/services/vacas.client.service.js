'use strict';

// Vacas service used to communicate Vacas REST endpoints
angular.module('vacas').factory('Vacas', ['$resource',
	function($resource) {
		return $resource('vacas/:vacaId', { vacaId: '@_id'
	}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('VacaPorRP', ['$resource',
    function($resource){
      return $resource('vacas' ,{
        query: {
            method: 'GET',
						isArray: false
        }
      });
    }
]);